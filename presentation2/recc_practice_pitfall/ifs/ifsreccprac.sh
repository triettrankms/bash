#!/bin/bash

#This script demonstrates one way to change the field delimiter from source to a space to avoid th need to manipulate the IFS variable

#Global VARs
COUNT="1"

#Script starts

while read -r CPU RAM DISK; do
    echo Machine "$COUNT": CPU is "$CPU" -- RAM is "$RAM" -- DISK is "$DISK"
    COUNT=$(( "$COUNT" + 1 ))
done <<< $( cat input | tr ',' ' '   )

for VAR in $( ls -l input ); do
    echo item1: "$VAR"
done;