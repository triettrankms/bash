#!/bin/bash

#This script demonstrates the subshell behaviour. Shell variable manipulations inside subshell are only visible inside the subshell. 
#The subshell can be created when pipe is used

#Global VARs
TEMP="default"

#Script starts

cat "input" | while true -r LINE; do
    TEMP="$LINE"
    echo Value of TEMP in side loop: "$TEMP"
done;

#echo Value of TEMP outside loop: "$TEMP";
